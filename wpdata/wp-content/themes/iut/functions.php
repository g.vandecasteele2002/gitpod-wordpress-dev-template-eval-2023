<?php
add_action( 'wp_enqueue_scripts', 'iut_enqueue_styles' );
function iut_enqueue_styles() {
	$parenthandle = 'parent-style'; // This is 'twentynineteen-style' for the Twenty Nineteen theme.
	$theme        = wp_get_theme();
	wp_enqueue_style( $parenthandle,
		get_template_directory_uri() . '/style.css',
		array(),  // If the parent theme code has a dependency, copy it to here.
		$theme->parent()->get( 'Version' )
	);
	wp_enqueue_style( 'child-style',
		get_stylesheet_uri(),
		array( $parenthandle ),
		$theme->get( 'Version' ) // This only works if you have Version defined in the style header.
	);
}

function iut_register_post_type() {

	register_post_type(
		'recette',
		array(
			'labels'				=> array(
				'name'					=> 'Recettes',
				'singular_name'			=> 'Recette',
			),
			'public'				=> true,
			'publicly_queryable'	=> true,   
			'show_in_rest'			=> true,
			'hierarchical'			=> false,
			'supports'				=> array( 'title', 'editor', 'thumbnail' ),
			'has_archive'			=> 'recettes',
			'rewrite'				=> array( 'slug' => 'recipe' ),
		)
	);


}

add_action( 'init', 'iut_register_post_type', 11 );

function iut_add_meta_boxes_recette( $post ) {

	add_meta_box(
		'iut_mbox_recette',
		'Infos complémentaires',
		'iut_mbox_recette_content',
		'recette'
	);

}

add_action( 'add_meta_boxes', 'iut_add_meta_boxes_recette' );

function iut_mbox_recette_content( $post ) {

	// Get meta value
	$iut_ingredients = get_post_meta(
		$post->ID,
		'iut-ingredients',
		true
	);

	echo '<p>';
	echo '<label for="iut-ingredients">';
	echo 'Ingredients de la recette';
	echo '<input type="textarea" id="iut-ingredients" name="iut-ingredients" value="' . $iut_ingredients . '">';
	echo '</label>';
	echo '</p>';

}

// Save post meta
function iut_save_post( $post_id ) {

    if ( isset( $_POST['iut-ingredients'] ) && !empty( $_POST['iut-ingredients'] ) ) {

        update_post_meta(
            $post_id,
            'iut-ingredients',
            sanitize_text_field( $_POST['iut-ingredients'] )
        );

    }

}

add_action( 'save_post', 'iut_save_post' );

